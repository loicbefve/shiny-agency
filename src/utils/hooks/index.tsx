import { useEffect, useState } from 'react'

type UseFetchState<T> = {
  isLoading: boolean
  error?: Error
  data?: T
}

export function useFetch<T>(url: string, data_key: string) {
  const [state, setState] = useState<UseFetchState<T>>({
    isLoading: true,
  })

  useEffect(() => {
    if (!url) return

    async function fetchData() {
      try {
        const response = await fetch(url)
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`)
        }
        const data = await response.json()
        setState({ isLoading: false, data: data[data_key] })
      } catch (e: any) {
        setState({ isLoading: false, error: e })
      }
    }
    fetchData()
  }, [url])

  return state
}

import { createGlobalStyle } from 'styled-components'
import { useContext } from 'react'
import { IThemeContext, ThemeContext } from '../context'

const StyledGlobalStyle = createGlobalStyle<{ isDarkMode: boolean }>`
  * {
    font-family: 'Trebuchet MS', Helvetica, sans-serif;
  }

  body {
      /* Ici cette syntaxe revient au même que
        background-color: ${(props) =>
          props.isDarkMode ? '#2F2E41' : 'white'};
        */
    background-color: ${({ isDarkMode }) => (isDarkMode ? 'black' : 'white')};
    margin: 0;
  }
`

function GlobalStyle() {
  const theme = useContext<IThemeContext>(ThemeContext)
  return <StyledGlobalStyle isDarkMode={theme.theme === 'dark'} />
}

export default GlobalStyle

import styled from 'styled-components'
import { Link } from 'react-router-dom'

const StyledLink = styled(Link)<{ $isFullLink?: boolean }>`
  padding: 10px 15px;
  color: ${(props) => props.theme.colors.secondary};
  text-decoration: none;
  font-size: 18px;
  text-align: center;
  ${(props) =>
    props.$isFullLink &&
    `color: white;
     border-radius: 30px;
      background-color: ${props.theme.colors.primary}`};
`
export default StyledLink

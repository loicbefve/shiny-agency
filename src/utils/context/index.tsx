import { createContext, PropsWithChildren, useState } from 'react'

// THEME CONTEXT
export interface IThemeContext {
  theme: string
  toogleTheme: () => void
}
export const ThemeContext = createContext<IThemeContext>({
  theme: 'light',
  toogleTheme: () => {},
})

export const ThemeContextProvider = ({ children }: PropsWithChildren) => {
  const [theme, setTheme] = useState<string>('light')
  const toogleTheme = () => {
    setTheme(theme === 'light' ? 'dark' : 'light')
  }

  return (
    <ThemeContext.Provider value={{ theme, toogleTheme }}>
      {children}
    </ThemeContext.Provider>
  )
}

// SURVEY CONTEXT

export interface IAnswer {
  questionNumber: number
  answer: boolean
}

export interface ISurveyContext {
  answers: IAnswer[]
  saveAnswers: (newAnswer: IAnswer) => void
}
export const SurveyContext = createContext<ISurveyContext>({
  answers: [] as IAnswer[],
  saveAnswers: (_: IAnswer) => {},
})

export const SurveyProvider = ({ children }: PropsWithChildren) => {
  const [answers, setAnswers] = useState<IAnswer[]>([] as IAnswer[])
  const saveAnswers = (newAnswer: IAnswer) => {
    setAnswers([...answers, newAnswer])
  }

  return (
    <SurveyContext.Provider value={{ answers, saveAnswers }}>
      {children}
    </SurveyContext.Provider>
  )
}

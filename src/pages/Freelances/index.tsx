import Card from '../../components/Card'
import styled from 'styled-components'
import { useEffect, useState } from 'react'
import { Loader } from '../../utils/style/Loader'

const PageTitle = styled.h1`
  font-size: 30px;
  color: black;
  text-align: center;
  padding-bottom: 30px;
`

const PageSubtitle = styled.h2`
  font-size: 20px;
  color: ${(props) => props.theme.colors.secondary};
  font-weight: 300;
  text-align: center;
  padding-bottom: 30px;
`

const CardsContainer = styled.div`
  display: grid;
  gap: 24px;
  grid-template-rows: 350px 350px;
  grid-template-columns: repeat(2, 1fr);
  align-items: center;
  justify-items: center;
`

interface IResultData {
  id: string
  name: string
  job: string
  picture: string
  skills: string[]
  location: string
  available: boolean
  tjm: number
}

function Freelances() {
  const [freelanceProfiles, setFreelanceProfiles] = useState(
    [] as IResultData[]
  )
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    setIsLoading(true)
    fetch('http://localhost:8000/freelances')
      .then((response) => response.json())
      .then(({ freelancersList }) => {
        setFreelanceProfiles(freelancersList)
        setIsLoading(false)
      })
      .catch((error) => console.log(error))
  }, [])

  return (
    <div>
      <PageTitle>Trouvez votre prestataire</PageTitle>
      <PageSubtitle>
        Chez Shiny nous réunissons les meilleurs profils pour vous
      </PageSubtitle>
      {isLoading ? (
        <Loader data-testid="loader" />
      ) : (
        <CardsContainer>
          {freelanceProfiles.map((profile, index) => (
            <Card
              key={`${profile.name}-${index}`}
              label={profile.job}
              title={profile.name}
              picture={profile.picture}
            />
          ))}
        </CardsContainer>
      )}
    </div>
  )
}

export default Freelances

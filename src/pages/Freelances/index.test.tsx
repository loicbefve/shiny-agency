import { setupServer } from 'msw/node'
import { rest } from 'msw'
import { render, screen, waitFor } from '@testing-library/react'
import { ThemeContextProvider } from '../../utils/context'
import Freelances from './index'
import { ThemeProvider } from 'styled-components'
import { defaultTheme } from '../../utils/style/theme'
import { PropsWithChildren } from 'react'

const freelancersMockedData = [
  {
    name: 'Harry Potter',
    job: 'Magicien frontend',
    picture: '',
  },
  {
    name: 'Hermione Granger',
    job: 'Magicienne fullstack',
    picture: '',
  },
]

function Wrapper({ children }: PropsWithChildren) {
  return (
    <ThemeProvider theme={defaultTheme}>
      <ThemeContextProvider>{children}</ThemeContextProvider>
    </ThemeProvider>
  )
}

const server = setupServer(
  rest.get('http://localhost:8000/freelances', (req, res, ctx) => {
    return res(ctx.json({ freelancersList: freelancersMockedData }))
  })
)

beforeAll(() => server.listen())

afterEach(() => server.resetHandlers())

afterAll(() => server.close())

test('Should render without crash', async () => {
  render(<Freelances />, { wrapper: Wrapper })
  expect(screen.getByTestId('loader')).toBeTruthy()
})

it('Should display freelancers names', async () => {
  render(<Freelances />, { wrapper: Wrapper })

  expect(screen.getByTestId('loader')).toBeTruthy()

  const assertHarryPotterIsPresent = () => {
    expect(screen.getByText('Harry Potter')).toBeTruthy()
  }

  const assertHermioneGrangerIsPresent = () => {
    expect(screen.getByText('Hermione Granger')).toBeTruthy()
  }

  await waitFor(() => {
    assertHarryPotterIsPresent()
    assertHermioneGrangerIsPresent()
  })
})

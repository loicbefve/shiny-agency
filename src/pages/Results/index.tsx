import { useContext } from 'react'
import { SurveyContext } from '../../utils/context'

export function formatJobList(
  title: string,
  listLength: number,
  index: number
) {
  if (index === listLength - 1) {
    return title
  }
  return `${title},`
}
function Results() {
  const { answers } = useContext(SurveyContext)
  console.log(answers)

  return <div>This is the result page</div>
}

export default Results

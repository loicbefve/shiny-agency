import { Link, useParams } from 'react-router-dom'
import Error from '../components/Error'
import styled from 'styled-components'
import { useContext, useState } from 'react'
import { Loader } from '../utils/style/Loader'
import { IAnswer, SurveyContext } from '../utils/context'
import { useFetch } from '../utils/hooks'

const SurveyContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const QuestionTitle = styled.h2`
  text-decoration: underline;
  text-decoration-color: ${(props) => props.theme.colors.primary};
`

const QuestionContent = styled.span`
  margin: 30px;
`

const LinkWrapper = styled.div`
  padding-top: 30px;
  & a {
    color: black;
  }
  & a:first-of-type {
    margin-right: 20px;
  }
`

const ReplyBox = styled.button<{ isSelected: boolean }>`
  border: none;
  height: 100px;
  width: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => props.theme.colors.backgroundLight};
  border-radius: 30px;
  cursor: pointer;
  box-shadow: ${(props) =>
    props.isSelected
      ? `0px 0px 0px 2px ${props.theme.colors.primary} inset`
      : 'none'};
  &:first-child {
    margin-right: 15px;
  }
  &:last-of-type {
    margin-left: 15px;
  }
`

const ReplyWrapper = styled.div`
  display: flex;
  flex-direction: row;
`

function findQuestion(
  questionNumber: number,
  surveyQuestions: ResultData[] | undefined
): string | undefined {
  const maybeQuestion = surveyQuestions?.find(
    (q) => q.questionId === questionNumber
  )
  return maybeQuestion?.question
}

function findAnswer(
  questionNumber: number,
  answers: IAnswer[]
): boolean | undefined {
  const maybeanswer = answers.find((a) => a.questionNumber === questionNumber)
  if (maybeanswer) {
    return maybeanswer.answer
  } else {
    return undefined
  }
}

type ResultData = {
  questionId: number
  question: string
}

function Survey() {
  const { questionNumber } = useParams()
  const [error, setError] = useState(false)
  const { answers, saveAnswers } = useContext(SurveyContext)
  const { data, isLoading } = useFetch<ResultData[]>(
    `http://localhost:8000/survey`,
    'questions'
  )

  function saveReply(questionNumber: number, answer: boolean) {
    saveAnswers({
      questionNumber: questionNumber,
      answer: answer,
    })
  }

  // useEffect(() => {
  //   setDataLoading(true)
  //   fetch(`http://localhost:8000/survey`)
  //     .then((response) => response.json())
  //     .then(({ questions }) => {
  //       setSurveyQuestions(questions)
  //       setDataLoading(false)
  //     })
  //     .catch((_) => setError(true))
  // }, [])

  if (!questionNumber || +questionNumber < 1 || +questionNumber > 10) {
    return <Error />
  } else if (error) {
    return <span>Oops something went wrong !</span>
  } else {
    const questionNumberInt = parseInt(questionNumber)
    const isPreviousActive = questionNumberInt > 1
    const isNextActive = findQuestion(questionNumberInt + 1, data)

    return (
      <SurveyContainer>
        <QuestionTitle>Question {questionNumber}</QuestionTitle>
        {isLoading ? (
          <Loader />
        ) : (
          <QuestionContent>
            {findQuestion(questionNumberInt, data)}
          </QuestionContent>
        )}
        <ReplyWrapper>
          <ReplyBox
            onClick={() => saveReply(questionNumberInt, true)}
            isSelected={findAnswer(questionNumberInt, answers) === true}
          >
            Oui
          </ReplyBox>
          <ReplyBox
            onClick={() => saveReply(questionNumberInt, false)}
            isSelected={findAnswer(questionNumberInt, answers) === false}
          >
            Non
          </ReplyBox>
        </ReplyWrapper>

        <LinkWrapper>
          {isPreviousActive && (
            <Link to={`/survey/${questionNumberInt - 1}`}>
              Previous question
            </Link>
          )}
          {isNextActive ? (
            <Link to={`/survey/${questionNumberInt + 1}`}>Next question</Link>
          ) : (
            <Link to="/results">Résultats</Link>
          )}
        </LinkWrapper>
      </SurveyContainer>
    )
  }
}

export default Survey

import React from 'react'
import ReactDOM from 'react-dom/client'
import Home from './pages/Home/index'
import Survey from './pages/Survey'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Header from './components/Header'
import Error from './components/Error'
import Results from './pages/Results'
import Freelances from './pages/Freelances'
import { ThemeProvider } from 'styled-components'
import { defaultTheme } from './utils/style/theme'
import Footer from './components/Footer'
import { SurveyProvider, ThemeContextProvider } from './utils/context'
import GlobalStyle from './utils/style/GlobalStyle'

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

root.render(
  <React.StrictMode>
    <ThemeContextProvider>
      <SurveyProvider>
        <ThemeProvider theme={defaultTheme}>
          <BrowserRouter>
            <GlobalStyle />
            <Header />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/results" element={<Results />} />
              <Route path="/freelances" element={<Freelances />} />
              <Route path="/survey/:questionNumber" element={<Survey />} />
              <Route path="*" element={<Error />} />
            </Routes>
            <Footer />
          </BrowserRouter>
        </ThemeProvider>
      </SurveyProvider>
    </ThemeContextProvider>
  </React.StrictMode>
)

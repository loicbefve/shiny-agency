import styled from 'styled-components'
import { useContext } from 'react'
import { ThemeContext } from '../../utils/context'

const FooterContainer = styled.footer`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding-top: 60px;
`

const NightModeButton = styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;
  color: ${(props) => props.theme.colors.secondary};
`

function Footer() {
  const { theme, toogleTheme } = useContext(ThemeContext)
  return (
    <FooterContainer>
      <NightModeButton onClick={() => toogleTheme()}>
        Changer de mode : {theme === 'light' ? '☀️' : '🌙'}
      </NightModeButton>
    </FooterContainer>
  )
}

export default Footer

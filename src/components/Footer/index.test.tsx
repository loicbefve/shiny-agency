import { fireEvent, render, screen } from '@testing-library/react'
import Footer from './index'

import { ThemeContextProvider } from '../../utils/context'
import { ThemeProvider } from 'styled-components'
import { defaultTheme } from '../../utils/style/theme'

describe('Footer testing', () => {
  it('should render without crash', async () => {
    render(
      <ThemeProvider theme={defaultTheme}>
        <ThemeContextProvider>
          <Footer />
        </ThemeContextProvider>
      </ThemeProvider>
    )
    const nightModeButton = screen.getByRole('button')
    expect(nightModeButton.textContent).toBe('Changer de mode : ☀️')
    fireEvent.click(nightModeButton)
    expect(nightModeButton.textContent).toBe('Changer de mode : 🌙')
  })
})

import styled from 'styled-components'
import logo from '../../assets/dark-logo.png'
import StyledLink from '../../utils/style/Atom'

const HomeLogo = styled.img`
  height: 70px;
`

const NavContainer = styled.nav`
  padding: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

function Header() {
  return (
    <NavContainer>
      <StyledLink to="/">
        <HomeLogo src={logo} alt="shiny_agency_logo" />
      </StyledLink>
      <div>
        <StyledLink to="/">Accueil</StyledLink>
        <StyledLink to="/freelances">Freelances</StyledLink>
        <StyledLink to="/survey/1" $isFullLink>
          Faire le test
        </StyledLink>
      </div>
    </NavContainer>
  )
}

export default Header

# Shiny agency (tutoriel) :

## Présentation :

Ce site web écrit en React est le résultat du tutoriel openclassroom :  https://openclassrooms.com/fr/courses/7150606-creez-une-application-react-complete
La particularité est que j'ai décidé, contrairement au tutoriel d'utiliser le **Typescript** plutôt que le **JavaScript**.


Il s'agit d'un site d'agence de recrutement de freelances.'

Les concepts React vues sont :
- Le react-router
- Les styled-components
- Le contexte avec useContext
- Le hook useEffect pour les call API
- Les tests unitaires et d'intégration

## Résultat :

![result.png](result.png)

## Certificat de réussite :

![certificat.png](certificat.png)
